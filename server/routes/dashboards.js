const router = require("express").Router();
const multer = require("multer");
const pool = require("../db");
const path = require("path");
const uuid = require("uuid").v4;
var fs = require('fs');
const authorization = require("../middleware/authorization");
const validInfo = require("../middleware/validinfo");

//tables

router.get("/table/rawdata", async(req, res) => {
    const rawdata = await pool.query("SELECT * FROM chart4_dtms order by ts DESC")

    return res.send({
        res: rawdata.rows
    })
})

//bendpoints or device

router.get("/device", async(req,res) => {
    try {
        
        const post = await pool.query("SELECT * from bendpoint_dtms order by lastupdate DESC");
        res.json(post.rows);
    } catch (err) {
        console.error(err.message);
        res.status(500).json("Server Error");
    }
});
//maintenance

router.post("/contacts/add", async(req, res)=> {

        const { fullname, email, phonenumber, area, sector} = req.body

        if(![fullname, email, phonenumber, area, sector].every(Boolean))
        {
            return res.status(401).send({
                error: "Please complete details!",
                body: req.body
            });
        }
        
        const alert =  await pool.query ("SELECT * from contacts_dtms WHERE phonenumber = $1",[phonenumber])

        if( alert.rows.length !== 0) {
            return res.status(401).send( {
                error: "Phone number already Exist"
            })
        }
        const newNo = await pool.query("INSERT INTO contacts_dtms (fullname,email,phonenumber,area,sector,dateregistered) VALUES ($1,$2,$3,$4,$5,now()) RETURNING *",[fullname,email,phonenumber,area,sector])
        res.json(newNo.rows[0]);
});



router.put("/contacts/edit", async(req, res)=> {

        const { id, fullname, email, phonenumber, area, sector} = req.body

        if(![fullname, email, phonenumber, area, sector].every(Boolean))
        {
            return res.json({
                error: "Please complete details!",
            });
        }
        
        const alert =  await pool.query ("SELECT * from contacts_dtms WHERE phonenumber = $1 and id != $2",[phonenumber, id])
    
        if( alert.rows.length !== 0) {
            return res.status(401).json({
                error: "Phone number already exist in another user!",
            });
        }
        const newNo = await pool.query("UPDATE contacts_dtms SET fullname = $1, email= $2, phonenumber=$3, area=$4, sector=$5 where id =$6  RETURNING *",[fullname,email,phonenumber,area,sector, id])
        res.json(newNo.rows[0]);
});

//ROUTER POST

router.post("/post/add", async(req,res) => {
 
    
    try {
        const storage = multer.diskStorage({
            destination: './storage/upload/post/',
            filename: function(req, files, cb) {
                cb (null, `${uuid()}${path.extname(files.originalname)}`);
            },
            details: function (req, details, cb) {
                cb(null, details);
            }
        })
        const upload = multer({storage: storage}).any();
        
        upload(req , res, async (err) => {
            if(err) {

            }else {
                const {serialno, tln, tcn, circuitno, area,sector,lat,lng,loc,installed,energized,installedby,energizedby,lastactiveby,active,kva } = JSON.parse(req.body.details);         
                if(![serialno, tln, tcn, circuitno, area,sector,lat,lng,loc,installed,energized,installedby,energizedby,lastactiveby,active,kva].every(Boolean))
                 {
                     return res.status(401).send(
                         {
                             error: "Please complete details!"
                         });
                 }
                const user = await pool.query("SELECT * FROM Loc_DTMS WHERE tln = $1 and active = true",[tln]);
                
                if(user.rows.length!==0)
                {
                    return res.status(401).json({"status":401,"error":"TLN already exist!"});
                }
          
                var ImgUrl = 'storage/upload/post/'+ req.files[0].filename
                const newUnit = await pool.query("INSERT INTO Loc_DTMS (serialno, tln, tcn, circuitno, area,sector,lat,lng,loc,installed,energized,lastactive,installedby,energizedby,lastactiveby,status,kva,voltage,percentload,temp,img,active,atemp,ctemp) VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,now(),$12,$13,$14,false,$15,0,0,0,$16,$17,0,0) RETURNING *",[serialno, tln.toUpperCase(), tcn, circuitno, area.toUpperCase(),sector.toUpperCase(),lat,lng,loc,installed,energized,installedby,energizedby,lastactiveby,kva,ImgUrl,active]);        
                res.json(newUnit.rows[0]);
            }
        })
    } catch (error) {
        console.error(error.message);
        res.status(500).send({
            error: "Server Error"
        });
    }
});
    router.put("/post/edit", async(req, res) => {

        const storage = multer.diskStorage({
            destination: './storage/upload/post/',
            filename: function(req, files, cb) {
                cb (null, `${uuid()}${path.extname(files.originalname)}`);
            },
            details: function (req, details, cb) {
                cb(null, details);
            }
        })
        const upload = multer({storage: storage}).any();

        upload(req , res, async (error) => {
            if(error) {
                res.status(500).send({
                    error: error
                })
            }else {
       
                const {serialno, tln, tcn, circuitno, area,sector,lat,lng,loc,installed,energized,installedby,energizedby,lastactiveby,active,id,kva } = JSON.parse(req.body.details);
                const post = await pool.query("SELECT * FROM Loc_DTMS WHERE id = $1 and active = true",[id]);
                var imgUrl = null
                //if the text field file have input
                if(req.files[0] !== undefined){
                    var imgUrl = 'storage/upload/post/'+ req.files[0].filename
                }else {
                    if(post.rows[0].img !== null){
                        imgUrl = post.rows[0].img
                    }
                }
                  const updatePost = await pool.query("UPDATE Loc_DTMS SET serialno = $1, tln = $2, tcn = $3, circuitno= $4, area = $5, sector = $6, lat=$7, lng = $8, loc=$9, installed=$10, energized = $11, installedby=$12, energizedby=$13, lastactiveby=$14, active =$15, img =$16, kva= $18 WHERE id = $17 RETURNING *",
                [serialno, tln, tcn, circuitno, area,sector,lat,lng,loc,installed,energized,installedby,energizedby,lastactiveby,active,imgUrl, id, kva]);
                res.json(updatePost.rows[0])
            }       
        })
    });

//ROUTER GET
router.get("/",authorization, async(req,res) => {
    try {
        
        const user = await pool.query("SELECT * FROM User_DTMS WHERE id =$1",[req.user]);
        res.json(user.rows[0]);
    } catch (err) {
        console.error(err.message);
        res.status(500).json("Server Error");
    }
});

router.get("/post", async(req,res) => {
    try {
        
        const post = await pool.query("SELECT * FROM Loc_DTMS where active = true order by id DESC");
    
        let data = []
        for (var i = 0; i < post.rowCount; i++){
            let idt = new Date(post.rows[i].installed.toString())
            let residt =  new Date(idt.getTime() - (idt.getTimezoneOffset() * 60000 ))
            .toISOString()
            .split("T")[0];
            let edt = new Date(post.rows[i].energized.toString())
            let resedt = new Date(edt.getTime() - (edt.getTimezoneOffset() * 60000 ))
            .toISOString()
            .split("T")[0];
            let ldt = new Date(post.rows[i].lastactive.toString())
            resldt = new Date(ldt.getTime() - (ldt.getTimezoneOffset() * 60000 ))
            .toISOString()
            .split("T")[0];
             data.push({
               id: post.rows[i].id,
               serialno: post.rows[i].serialno,
               tln : post.rows[i].tln,
               tcn: post.rows[i].tcn,
               circuitno: post.rows[i].circuitno,
               area: post.rows[i].area,
               sector: post.rows[i].sector,
               lat: post.rows[i].lat,
               lng: post.rows[i].lng,
               loc: post.rows[i].loc,
               installed: residt,
               energized: resedt,
               lastactive: ldt,
               installedby: post.rows[i].installedby,
               energizedby: post.rows[i].energizedby,
               lastactiveby: post.rows[i].lastactiveby,
               status: post.rows[i].status,
               active: post.rows[i].active,
               percentload: post.rows[i].percentload,
               kva: post.rows[i].kva,
               voltage: post.rows[i].voltage,
               temp: post.rows[i].temp,
               atemp: post.rows[i].atemp,
               ctemp: post.rows[i].ctemp,
               img: fs.readFileSync((post.rows[i].img !== null ? post.rows[i].img : 'storage/upload/noimage.png'), 'base64')  
             }) 
        }
        res.json(data);
    } catch (err) {
        console.error(err.message);
        res.status(500).send({
            error: err.message
        });
    }
});

router.get("/test", async(req,res) => {
    try {
        
        const post = await pool.query("SELECT tln,installed,extract(day from now()::timestamp - installed::timestamp) as age,extract(day from (installed + justify_interval(interval '20 year'))::timestamp - installed::timestamp) as maxlife,(extract(day from now()::timestamp - installed::timestamp) / extract(day from (installed + justify_interval(interval '20 year'))::timestamp - installed::timestamp))*100 as agepercent FROM public.loc_dtms");
        res.json(post.rows);
    } catch (err) {
        console.error(err.message);
        res.status(500).json("Server Error");
    }
});
router.get("/anatomy", async(req,res) => {
    try {
        var url = 'https://www.figma.com/proto/zkfHDmRGiGvQOttvzxIVSY/Ionics?node-id=125%3A179&viewport=1214%2C575%2C0.3661139905452728&scaling=scale-down'
        res.json(url)
      
    } catch (err) {
        console.error(err.message);
        res.status(500).json("Server Error");
    }
});

router.get("/source/:id", async(req, res) => {
    try{
        const UrlImage = await pool.query("SELECT * FROM Loc_DTMS WHERE id = $1 and active = true", [req.params.id]);
        var img = UrlImage.rows[0].img
        var imageAsBase64 = fs.readFileSync((img !== null ? img : 'storage/upload/noimage.png'), 'base64');
        res.send({
            img : imageAsBase64,
        })
        // res.sendFile('app.vue', { root: path.join(__dirname, 'storage/upload/post/'+ UrlImage) })
    } catch (err) {
        console.error(err.message);
        res.status(500).json("Server Error");
    }
   
});


router.get("/analytics/age/:tln", async(req,res) => {
    try {
        
        const post = await pool.query("SELECT tln,installed,extract(day from now()::timestamp - installed::timestamp) as age,extract(day from (installed + justify_interval(interval '20 year'))::timestamp - installed::timestamp) as maxlife,(extract(day from now()::timestamp - installed::timestamp) / extract(day from (installed + justify_interval(interval '20 year'))::timestamp - installed::timestamp))*100 as agepercent FROM public.loc_dtms where tln = $1 and active = true",[req.params.tln]);
        res.json(post.rows);
    } catch (err) {
        console.error(err.message);
        res.status(500).json("Server Error");
    }
});

router.get("/post/:tln", async(req,res) => {
    try {
        
        const post = await pool.query("SELECT * FROM Loc_DTMS where tln = $1 and active = true",[req.params.tln]);
        res.json(post.rows);
    } catch (err) {
        console.error(err.message);
        res.status(500).json("Server Error");
    }
});

router.get("/contacts", async(req,res) => {
    try {
        
        const contacts = await pool.query("SELECT * FROM contacts_DTMS");
        res.json(contacts.rows);
    } catch (err) {
        console.error(err.message);
        res.status(500).json("Server Error");
    }
});

router.get("/time", async(req,res) => {
    try {
        
        const time = await pool.query("SELECT NOW() as time");
        res.json(time.rows[0]);
    } catch (err) {
        console.error(err.message);
        res.status(500).json("Server Error");
    }
});

module.exports = router;