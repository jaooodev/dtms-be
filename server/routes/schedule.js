const router = require("express").Router();
const pool = require("../db");
const schedule = require('node-schedule');
var rule = new schedule.RecurrenceRule();
rule.minute = 0;

// Hourly Jobs
var thermaljobs = schedule.scheduleJob(rule, async function(){
    var d = new Date()
    var to = d.getFullYear() + '-' + (d.getMonth() + 1) + '-' + d.getDate() + ' ' + d.getHours() + ':00:00'
    var from = d.getHours() !== 0 ? d.getFullYear() + '-' + (d.getMonth() + 1) + '-' + d.getDate() + ' ' + (d.getHours()-1) + ':00:00' : d.getFullYear() + '-' + (d.getMonth() + 1) + '-' + (d.getDate()-1) + ' ' + '23:00:00'
    console.log('Thermal life hourly calculation is processing')
    console.log('Time and date: ' + from + ' to ' + to)
    const tlnlife = await pool.query("SELECT a.tln, ROUND(((avg(a.appowertotal)) / b.kva) * 100) as percentload,ROUND(avg(a.ambient_temp)) as ambient FROM public.chart4_dtms a inner join loc_dtms b on a.tln=b.tln where a.ts between now()  - interval '1 hour' and now() group by a.TLN,b.kva;")
    for(var i = 0;i< tlnlife.rowCount;i++)
    {
        var temp1 = (65*(Math.pow((tlnlife.rows[i].percentload/100),0.8)))
        var est = Math.round(temp1) + Math.round(tlnlife.rows[i].ambient) + 20
        var agefactor
        if(Math.round(est)>36)
        {
            af = await pool.query("SELECT * from agefactor_dtms where temp = '"+Math.round(est)+"'")
            if(af.rowCount > 0)
            {
                agefactor = af.rows[0].agefactor
            }
            else
            {
                agefactor = 0
            }
            
        }
        else{
            agefactor = 0
        }
        
        pool.query("INSERT INTO thermalage_dtms (tln,ambient_temp,percentload,esthotspot,datefrom,dateto,lifeconsumed,recorddate) VALUES ($1,$2,$3,$4,DATE_TRUNC('hour', now() - interval '1 hour'),DATE_TRUNC('hour', now()),$5,now()) RETURNING *",[tlnlife.rows[i].tln,tlnlife.rows[i].ambient,tlnlife.rows[i].percentload,Math.round(est),agefactor]);
        console.log(agefactor)
    }

});

var energyjobs = schedule.scheduleJob(rule, async function(){
    var d = new Date()
    var to = d.getFullYear() + '-' + (d.getMonth() + 1) + '-' + d.getDate() + ' ' + d.getHours() + ':00:00'
    var from = d.getHours() !== 0 ? d.getFullYear() + '-' + (d.getMonth() + 1) + '-' + d.getDate() + ' ' + (d.getHours()-1) + ':00:00' : d.getFullYear() + '-' + (d.getMonth() + 1) + '-' + (d.getDate()-1) + ' ' + '23:00:00'
    console.log('Energy consumed hourly calculation is processing')
    console.log('Time and date: ' + from + ' to ' + to)
    const tlnconsumed = await pool.query("select a.tln,sum(a.appowertotal) as consumed,sum(b.kva) as delivered from chart4_dtms a inner join loc_dtms b on a.tln=b.tln where a.ts between now()  - interval '1 hour' and now() group by a.tln;")
    

    for(var i = 0;i< tlnconsumed.rowCount;i++)
    {
        pool.query("INSERT INTO energyconsumed_dtms (tln,delivered,consumed,datefrom,dateto,recorddate) VALUES ($1,$2,$3,DATE_TRUNC('hour', now() - interval '1 hour'),DATE_TRUNC('hour', now()),now()) RETURNING *",[tlnconsumed.rows[i].tln,tlnconsumed.rows[i].delivered,tlnconsumed.rows[i].consumed]);
    }
});

//update TLN status every 5 minutes

    var idletln = schedule.scheduleJob("*/5 * * * *", async function() {
        const idle = await pool.query("select distinct(tln) from public.chart4_dtms where ts between now() - interval '5 minute' and now();");
        var data = idle.rows
        var result = data.length === 0 ? '' : data.map(function(val) {return "'"+val.tln+"'";}).join(',');
        
        pool.query("UPDATE loc_dtms set status=false where tln not in ($1)",[result])
        
        console.log('Status of all TLN is now updated')
        return 'test 1'
    });

module.exports = router;
module.exports.idletln = idletln