const router = require("express").Router();
const pool = require("../db");
const authorization = require("../middleware/authorization");
const broker = require("../middleware/broker");

//ROUTER POST
router.post("/",async (req,res) => {
    try {
        const {uuid} =req.body;

        const broker = await pool.query("SELECT * from bendpoint_DTMS where uuid = $1",[uuid]);



        res.send({
            TLN: broker.rows[0].tln,
            Phase: broker.rows[0].phase,
            Main_Topic: broker.rows[0].main_topic,
            MQTT:
                {
                    EndPoint: broker.rows[0].endpoint,
                    Port_Number: broker.rows[0].port,
                    Cert_Links: {
                        CA: broker.rows[0].ca,
                        Client_Cert: broker.rows[0].client_cert,
                        Client_Key: broker.rows[0].client_key,
                    },
                    Connection_Security: parseInt(broker.rows[0].con_sec)
                }
            ,
            Interval:[{
                MQTT: parseInt(broker.rows[0].mqtt_interval),
                HTTPS: parseInt(broker.rows[0].https_interval)
            }]
        });
    } catch (err) {
        console.error(err.message);
        res.status(500).send({
            error: "Server Error"
        });
    }
});



router.post("/register",broker, async(req,res) => {

    try {
        const {tln, phase, main_topic, endpoint, port,ca,client_cert,client_key,con_sec,mqtt_interval,https_interval,uuid } = req.body;
        const id = await pool.query("SELECT * FROM bendpoint_DTMS WHERE uuid = $1",[uuid]);
        if(id.rows.length!==0)
        {
            return res.status(401).json({"status":401,"error":"UUID already exist!"});
        }
        const NewUUID = await pool.query("INSERT INTO bendpoint_DTMS (tln, phase, main_topic, endpoint, port,ca,client_cert,client_key,con_sec,mqtt_interval,https_interval,lastupdate,uuid) VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,now(),$12) RETURNING *",[tln, phase, main_topic, endpoint, port,ca,client_cert,client_key,con_sec,mqtt_interval,https_interval,uuid]);
        res.send({UUID: NewUUID.rows[0].uuid})
    } catch (err) {
        console.error(err.message);
        res.status(500).send("Server Error");
    }
});

router.put("/update", async(req, res) => {
    try {
        const {id, tln, phase, con_sec, mqtt_interval, https_interval, uuid, active} =req.body;

        if(![id, tln, phase, con_sec, mqtt_interval, https_interval, uuid].every(Boolean))
        {
            return res.json({
                error: "Please complete details!",
            });
        }
        const newDevice = await pool.query("UPDATE bendpoint_DTMS SET tln = $1, phase= $2, con_sec=$3, mqtt_interval=$4, https_interval=$5, uuid=$6, active =$7 where id =$8  RETURNING *",
                        [tln,phase,con_sec,mqtt_interval,https_interval,uuid, active,id])
        res.json(newDevice.rows[0]);
    } catch (error) {

    }
});


//ROUTER GET

router.get("/:account", async(req,res) => {
    try {
        
        const broker = await pool.query("SELECT * FROM brokerip_dtms where  account =$1",[req.params.account]);
        res.json(broker.rows);
    } catch (err) {
        console.error(err.message);
        res.status(500).json("Server Error");
    }
});


module.exports = router;