const router = require("express").Router();
const pool = require("../db");
const authorization = require("../middleware/authorization");
const validInfo = require("../middleware/validinfo");
const fetch = require("node-fetch")

//ROUTER POST



//ROUTER GET

router.get("/:latlng", async (req,res) => {
   const latlng = req.params.latlng.split(',');
   const lat = latlng[0];
   const lng = latlng[1];
   const api_url =`https://api.openweathermap.org/data/2.5/onecall?lat=${lat}&lon=${lng}&appid=fb784f3b2069f38574eaede0f8f92c3e`;
   const weather_response = await fetch(api_url);
   const json = await weather_response.json();
   res.json(json);
});



module.exports = router;