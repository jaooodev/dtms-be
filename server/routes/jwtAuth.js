const router = require("express").Router();
const pool = require("../db");
const bcrypt = require("bcrypt");
const jwtGenerator = require("../utils/jwtGenerator");
const validInfo = require("../middleware/validinfo");
const WAFSqreen = require("../middleware/WAFSqreen");
const authorization = require("../middleware/authorization");
const Sqreen = require('sqreen');

const create_user = function(username, password, callback) {
    CreateUser(username, password, function (user) {
        console.log(user)
        Sqreen.signup_track({ username: user.username });
        return callback(user);
    });
}

const auth_user = function (username, password, callback) {
    AuthenticateUser(username, password, function (loginSuccess, user) {
        Sqreen.auth_track(loginSuccess, { username: user.username });
        return callback(loginSuccess, user);
    });
}



//register
router.post("/register", async(req,res) => {

    try {
        const {user_name, user_fname, user_lname, user_email, user_password, permission } = req.body;
        console.log(req.body)
        
        const user = await pool.query("SELECT * FROM User_DTMS WHERE user_name = $1",[user_name]);
        
         if(user.rows.length!==0)
        {
            return res.send({
                status: 401,
                alert: "User already exist!"
            });
        }
        const saltRound = 10;
        const salt = await bcrypt.genSalt(saltRound);
        const bcryptPassword = await bcrypt.hash(user_password, salt);
        const newUser = await pool.query("INSERT INTO User_DTMS (user_name, user_fname, user_lname, user_email, user_password, permission, dateregistered, active) VALUES ($1,$2,$3,$4,$5,$6,now(), '1') RETURNING *",[user_name, user_fname, user_lname, user_email, bcryptPassword, permission]);
        Sqreen.signup_track({username: user_name})
        //create_user(user_name,bcryptPassword)
        const token = jwtGenerator(newUser.rows[0].id); 
        return res.send({
            status: 200,
            alert: "Successfully Registered!"
        });
    } catch (err) {
        console.error(err.message);
        res.status(500).send("Server Error");
    }
});

//login
router.post("/login", validInfo,WAFSqreen,async (req,res) => {
    try {
        const {user_name, user_password} =req.body;
        const user = await pool.query("SELECT * from User_DTMS where user_name = $1",[user_name]);
        if(user.rows.length===0)
        {
            Sqreen.auth_track(false,{username: user_name});
            return res.status(404).send({
                error: "Username or Password is Incorrect!"
            });
        }
        
        if(user.rows[0].active === 0) {
            Sqreen.auth_track(false,{username: user_name});
            return res.status(404).send({
                error: "Your account is not active!"
            })
        }

        const validPword = await bcrypt.compare(user_password, user.rows[0].user_password);

        if(!validPword)
        {
            Sqreen.auth_track(false,{username: user_name});
            return res.status(401).send({
                error: "Username or Password is Incorrect!"
            });
        }
        //console.log(Sqreen.identify(user_name))
        Sqreen.auth_track(validPword,{username: user_name});
        console.log(Sqreen.track('app.sqreen.host.login',{user_identifiers:user_name}));
        Sqreen.identify({ username: user_name });
        const token = jwtGenerator(user.rows[0].id);
        res.send({
            token:token,
            user: user.rows[0].id,
            auth: user.rows[0].permission,
        });
    } catch (err) {
        console.error(err.message);
        res.status(500).send({
            error: err.message
        });
    }
});

router.get("/isverify", authorization, async (req, res) => {
    try {
        res.json(true);

    } catch (err) {
        console.error(err.message);
        res.status(500).send("Server Error");
    }
});

router.get("/users", async (req, res) => {
    try {
        const users = await pool.query("SELECT * from User_DTMS");
        res.json(users.rows)

    } catch (err) {
        console.error(err.message);
        res.status(500).send("Server Error");
    }
});

router.post("/login2", validInfo,async (req,res) => {
    try {
        const {user_name, user_password} =req.body;
        const user = await pool.query("SELECT * from User_DTMS where user_name = '"+user_name+"' and user_password = '"+user_password+"'");
        console.log(user)
        if(user.rows.length===0)
        {
            return res.status(404).send({
                error: "Username or Password is Incorrect!"
            });
        }
        
        if(user.rows[0].active === 0) {
            
            return res.status(404).send({
                error: "Your account is not active!"
            })
        }

        //console.log(Sqreen.identify(user_name))
        const token = jwtGenerator(user.rows[0].id);
        res.send({
            token:token,
            user: user.rows[0].id,
            auth: user.rows[0].permission,
        });
    } catch (err) {
        console.error(err.message);
        res.status(500).send({
            error: err.message
        });
    }
});

module.exports = router;