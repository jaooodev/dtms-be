const router = require("express").Router();
const { connect } = require("mqtt");
//const pool = require("./db");
const fetch = require('node-fetch')
const axios = require('axios');
const fs = require('fs');
var caFile = fs.readFileSync('my_root_ca.pem');
var certFile = fs.readFileSync('client.pem');
var keyFile = fs.readFileSync('client.key'); 
const api_url =  "http://103.219.69.55:5000/broker/"; 
const accountSid = 'ACde230dd8b4e3dae03ddcca02b1510a41'
const authToken ='1f0612a73c7353fdb7a707f3ea9e4405'
const smsclient = require('twilio')(accountSid,authToken)
var sql = require("mssql");
        // config for your database
        var config = {
            user: 'cats',
            password: 'dogs',
            server: '103.219.69.30', 
            database: 'eFactory',
            "options": {
                "encrypt": true,
                "enableArithAbort": true
                }
        };
var MQTToptions={
    clientId:"Duane1",
    port:8883,
    host:'103.219.69.55',
    protocol:'mqtts',
    rejectUnauthorized : false,
    ca:caFile,
    cert: certFile,
    key: keyFile
}
var mqtt = require("mqtt");
var topic = "meralco/update/+/alarm";
var client = mqtt.connect(MQTToptions);

function smstext(num,mes)
{
    smsclient.messages.create({
        to: num,
        from: '+12516071545',
        body: mes
    })
    .then((message) => console.log(message))
}

async function getapi(url) { 
    client.on("message",async (topic,message) => {
        const body = { uuid: topic.split('/')[2] };
        const response = await fetch(url, {
                    method: "POST",
                    headers: {"Content-Type" : "application/json"},
                    body: JSON.stringify(body)
        });
        var data = await response.json(); 
        var mes =  JSON.parse(message)
        console.log(mes.alarm)
        const smsmes = mes.alarm === 'brownout' ? 'MQTT Alarm: Outage Detected \n TLN: ' + data.TLN + ' Time: ' + new Date(mes.ts*1000).toLocaleString(): 'MQTT Alarm: Over Temp \n TLN: ' + data.TLN +' Temp: '+ mes.temp +'°C Time: ' + new Date(mes.ts*1000).toLocaleString()
        sql.connect(config, async function (err) {
            var request = new sql.Request();
            axios
            .get('http://103.219.69.55:5000/dashboards/contacts')
            .then(async (res) => {
                const count = await request.query("SELECT count(*) as total from MQTT_SMSMessages where datecreated between  (SELECT DATEADD(MINUTE,-3,getdate())) and getdate() and sender ='"+data.TLN+"'")
                for(var i =0;i<res.data.length;i++)
                {
                    console.log(res.data[i].phonenumber)
                    if(count.recordset[0].total==0)
                    {
                        request.query("INSERT INTO MQTT_SMSMessages (PhoneNumber,Message,Sender,DateCreated,Sent) VALUES ('"+res.data[i].phonenumber+"','"+smsmes+"','"+data.TLN+"',getdate(),0)")
                        request.query('EXEC MQTT_ALARM');
                        request.query("EXEC MQTT_EMAIL '"+res.data[i].email+"','"+smsmes+"'");
                    }
                    //smstext(res.data[1].phonenumber,smsmes)
                    
                }
            })
            /* const count = await request.query('SELECT count(*) as total from MQTT_SMSMessages where sent = 0')
            for(var i = 0; i < count.recordset[0].total; i++)
            {
                request.query('EXEC MQTT_ALARM');
            } */
            
        })
   
        // connect to your database
        /* /sql.connect(config, function (err) {
            var request = new sql.Request();
            request.query('EXEC MQTT_ALARM');
        })  */
    });

client.on("connect", () => {
    client.subscribe(topic, () => {
    });
    
});

}
getapi(api_url); 

router.get("/all/", async (req, res) => {
    try {
            sql.connect(config, async function (err) {
            var request = new sql.Request();
            const count = await request.query("SELECT * from MQTT_SMSMessages");
            res.json(count);
        })

    } catch (err) {
        console.error(err.message);
        res.status(500).send("Server Error");
    }
});

router.get("/all/:lim", async (req, res) => {
    try {
            sql.connect(config, async function (err) {
            var request = new sql.Request();
            const count = await request.query("SELECT top ("+req.params.lim+") * from MQTT_SMSMessages");
            res.json(count);
        })

    } catch (err) {
        console.error(err.message);
        res.status(500).send("Server Error");
    }
});

router.get("/all/:lim/:min", async (req, res) => {
    try {
            sql.connect(config, async function (err) {
            var request = new sql.Request();
            const count = await request.query("SELECT top ("+req.params.lim+") * from MQTT_SMSMessages where datecreated between (SELECT DATEADD(MINUTE,"+req.params.min+",getdate())) and getdate()");
            res.json(count);
        })

    } catch (err) {
        console.error(err.message);
        res.status(500).send("Server Error");
    }
});

router.get("/all/:lim/:from/:to", async (req, res) => {
    try {
            sql.connect(config, async function (err) {
            var request = new sql.Request();
            const count = await request.query("SELECT top ("+req.params.lim+") * from MQTT_SMSMessages where datecreated between '"+req.params.from+"' and '"+req.params.to+"'");
            res.json(count);
        })

    } catch (err) {
        console.error(err.message);
        res.status(500).send("Server Error");
    }
});

router.get("/tln/:tln", async (req, res) => {
    try {
            sql.connect(config, async function (err) {
            var request = new sql.Request();
            const count = await request.query("SELECT * from MQTT_SMSMessages where sender ='"+req.params.tln+"'");
            res.json(count);
        })

    } catch (err) {
        console.error(err.message);
        res.status(500).send("Server Error");
    }
});

router.get("/tln/:tln/:lim", async (req, res) => {
    try {
            sql.connect(config, async function (err) {
            var request = new sql.Request();
            const count = await request.query("SELECT top ("+req.params.lim+") * from MQTT_SMSMessages where sender ='"+req.params.tln+"'");
            res.json(count);
        })

    } catch (err) {
        console.error(err.message);
        res.status(500).send("Server Error");
    }
});

router.get("/tln/:tln/:lim/:min", async (req, res) => {
    try {
            sql.connect(config, async function (err) {
            var request = new sql.Request();
            const count = await request.query("SELECT top ("+req.params.lim+") * from MQTT_SMSMessages where sender ='"+req.params.tln+"' and datecreated between (SELECT DATEADD(MINUTE,"+req.params.min+",getdate())) and getdate()");
            res.json(count);
        })

    } catch (err) {
        console.error(err.message);
        res.status(500).send("Server Error");
    }
});

router.get("/tln/:tln/:lim/:from/:to", async (req, res) => {
    try {
            sql.connect(config, async function (err) {
            var request = new sql.Request();
            const count = await request.query("SELECT top ("+req.params.lim+") * from MQTT_SMSMessages where datecreated between '"+req.params.from+"' and '"+req.params.to+"' and sender = '"+req.params.tln+"'");
            res.json(count);
        })

    } catch (err) {
        console.error(err.message);
        res.status(500).send("Server Error");
    }
});


module.exports = router;