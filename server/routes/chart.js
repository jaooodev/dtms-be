const router = require("express").Router();
const pool = require("../db");
const authorization = require("../middleware/authorization");
const validInfo = require("../middleware/validinfo");

//ROUTER POST



//ROUTER GET

router.get("/daily/:time/:id/:lim", async(req,res) => {
    try {
        //ts > now() - interval '24 hour'  and
        const chart = await pool.query("SELECT * FROM chart_dtms where ts > now() - interval '"+req.params.time+" minutes' and  tln =$1 order by ts desc LIMIT $2",[req.params.id,req.params.lim]);
         // const currentp1  =
        // {
        //     data: chart.rows.map(({ts, currentp1,currentp2,currentp3}) => ({ts, currentp1}))
        // }
         res.send({
            data: chart.rows
        }); 
        //res.send(req.params)
    } catch (err) {
        console.error(err.message);
        res.status(500).json("Server Error");
    }
});

router.get("/daily/:id", async(req,res) => {
    try {
        //ts > now() - interval '24 hour'  and
        const chart = await pool.query("SELECT * FROM chart_dtms WHERE tln =$1 order by ts LIMIT 12",[req.params.id]);
         // const currentp1  =
        // {
        //     data: chart.rows.map(({ts, currentp1,currentp2,currentp3}) => ({ts, currentp1}))
        // }
         res.send({
            data: chart.rows
        }); 
        //res.send(req.params)
    } catch (err) {
        console.error(err.message);
        res.status(500).json("Server Error");
    }
});



router.get("/history/:from/:to/:id/:lim", async(req,res) => {
    try {
        
        const chart = await pool.query("SELECT * FROM chart_dtms  where ts between $2 and $3 and tln =$1 order by ts desc limit $4",[req.params.id,req.params.from,req.params.to,req.params.lim]);
        res.send({
            data: chart.rows
        });
    } catch (err) {
        console.error(err.message);
        res.status(500).json("Server Error");
    }
});

router.get("/history2/:from/:to/:id/:lim", async(req,res) => {
    try {
        
        const chart = await pool.query("SELECT * FROM chart4_dtms  where ts between $2 and $3 and tln =$1 order by ts desc limit $4",[req.params.id,req.params.from,req.params.to,req.params.lim]);
        res.send({
            data: chart.rows
        });
    } catch (err) {
        console.error(err.message);
        res.status(500).json("Server Error");
    }
});

router.get("/daily2/:time/:id/:lim", async(req,res) => {
    try {
        //ts > now() - interval '24 hour'  and
        const chart = await pool.query("SELECT * FROM chart4_dtms where ts > now() - interval '"+req.params.time+" minutes' and  tln =$1 order by ts desc LIMIT $2",[req.params.id,req.params.lim]);
         // const currentp1  =
        // {
        //     data: chart.rows.map(({ts, currentp1,currentp2,currentp3}) => ({ts, currentp1}))
        // }
         res.send({
            data: chart.rows
        }); 
        //res.send(req.params)
    } catch (err) {
        console.error(err.message);
        res.status(500).json("Server Error");
    }
});

router.get("/load/:id", async(req,res) => {
    try {
        const chart = await pool.query("SELECT tln, status,sum(duration) as seconds FROM public.logs_dtms where lastupdate > now() - interval '24 hour' and  tln =$1 group by tln,status;",[req.params.id]);
         res.send({
            data: chart.rows
        }); 
    } catch (err) {
        console.error(err.message);
        res.status(500).json("Server Error");
    }
});

router.get("/vload/:id", async(req,res) => {
    try {
        const chart = await pool.query("SELECT   tln, CASE WHEN ((voltage / 240) * 100) > 110 THEN 'Above 10% Nominal' WHEN ((voltage / 240) * 100) <= 110 AND ((voltage / 240) * 100) > 105 THEN '+5% to +10% of Nominal' WHEN ((voltage / 240) * 100) <= 105 AND ((voltage / 240) * 100) > 95 THEN '+5% to -5% of Nominal' WHEN ((voltage / 240) * 100) <= 95 AND ((voltage / 240) * 100) > 90 THEN '-5% to -10% of Nominal' WHEN ((voltage / 240) * 100) < 90 THEN '-10% Below of Nominal'  END voltageload,sum(duration) as seconds FROM public.logs_dtms where lastupdate > now() - interval '24 hour' and  tln=$1 group by tln,duration,voltageload order by voltageload asc",[req.params.id]);
         res.send({
            data: chart.rows
        }); 
    } catch (err) {
        console.error(err.message);
        res.status(500).json("Server Error");
    }
});

router.get("/thermal/:id", async(req,res) => {
    try {
        const chart = await pool.query("SELECT tln,(175320-(sum(lifeconsumed))) as liferemaining,(count(tln)) as total FROM public.thermalage_dtms where tln=$1 group by tln",[req.params.id]);
         res.send({
            data: chart.rows
        }); 
    } catch (err) {
        console.error(err.message);
        res.status(500).json("Server Error");
    }
});

router.get("/energy/:id", async(req,res) => {
    try {
        /* const chart = await pool.query("select * from energyconsumed_dtms where tln =$1 order by recorddate desc limit 1",[req.params.id]); */
        const chart = await pool.query("SELECT tln,sum(appowertotal) as delivered, (sum(appowertotal)) * 0.8 as consumed from chart4_dtms where tln =$1 and ts between DATE_TRUNC('MONTH', now()) and DATE_TRUNC('MONTH', now() + interval '1 month') group by tln",[req.params.id]);
         res.send({
            data: chart.rows
        }); 
    } catch (err) {
        console.error(err.message);
        res.status(500).json("Server Error");
    }
});

module.exports = router;