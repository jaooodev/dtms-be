const express = require("express");
const app = express();
const pool = require("./db");
const fetch = require('node-fetch');
const http = require('http');
const socketIo = require('socket.io')(5000);
const fs = require('fs');
var caFile = fs.readFileSync('my_root_ca.pem');
var certFile = fs.readFileSync('client.pem');
var keyFile = fs.readFileSync('client.key'); 


var options={
    clientId:"Duane",
    port:8883,
    host:'103.219.69.55',
    protocol:'mqtts',
    rejectUnauthorized : false,
    ca:caFile,
    cert: certFile,
    key: keyFile
}

const api_url =  "http://103.219.69.55:5000/broker/"; 
 
/* const { connect } = require("mqtt");
var mqtt = require("mqtt");
var client = mqtt.connect(options);
var topic = "/meralco/update/+/status";

client.on("message", (topic,message) => {
    io.on('connection', (socket) => {
        console.log(message.toString)
        socket.emit('active',message.toString)
    })
});

client.on("connect", () => {
    client.subscribe(topic);
});  */

 
socketIo.on('connection', (socket) => {
     function emit(stat,tln){
        socket.emit('active',{
            stat: stat,
            tln: tln
        })
    }
    const { connect } = require("mqtt");
    var mqtt = require("mqtt");
    var client = mqtt.connect(options);
    var topic = "/meralco/update/+/status";
    client.on("message", async (topic,message) => {
        const body = { uuid: topic.split("/")[3] };
        const response = await fetch(api_url, {
                    method: "POST",
                    headers: {"Content-Type" : "application/json"},
                    body: JSON.stringify(body)
        });
        var data = await response.json();
        console.log(JSON.parse(message),data.TLN)
        emit(JSON.parse(message),data.TLN)
    });

    client.on("connect", () => {
        client.subscribe(topic);
    }); 
})



/* io.on('connection', (socket) => {
    const { connect } = require("mqtt");
    var mqtt = require("mqtt");
    var client = mqtt.connect("mqtt://103.219.69.55:1234");
    var topic = "meralco/data";
    const pool = require("./db");
    client.on("message",(topic,message) => {
     const parseData = JSON.parse(message) 
         try {
            pool.query("INSERT INTO chart_DTMS (tln, main_topic, uuid, voltagep1, voltagep2, voltagep3, currentp1, currentp2, currentp3, faenergyp1, faenergyp2, faenergyp3, faenergytotal, frenergyp1, frenergyp2, frenergyp3, frenergytotal, rrenergyp1, rrenergyp2, rrenergyp3, rrenergytotal, aenergyp1, aenergyp2, aenergyp3, aenergyptotal, vthdp1, vthdp2, vthdp3, cthdp1, cthdp2, cthdp3, frequency, oil_temp, ambient_temp, onchip, ts) VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19,$20,$21,$22,$23,$24,$25,$26,$27,$28,$29,$30,$31,$32,$33,$34,$35,now()) RETURNING *",[parseData.TLN,topic,parseData.UUID,parseData.Voltage[0],parseData.Voltage[1],parseData.Voltage[2],parseData.Current[0],parseData.Current[1],parseData.Current[2],parseData.FAEnergy[0],parseData.FAEnergy[1],parseData.FAEnergy[2],parseData.FAEnergy[3],parseData.FREnergy[0],parseData.FREnergy[1],parseData.FREnergy[2],parseData.FREnergy[3],parseData.RREnergy[0],parseData.RREnergy[1],parseData.RREnergy[2],parseData.RREnergy[3],parseData.AEnergy[0],parseData.AEnergy[1],parseData.AEnergy[2],parseData.AEnergy[3],parseData.VTHD[0],parseData.VTHD[1],parseData.VTHD[2],parseData.CTHD[0],parseData.CTHD[1],parseData.CTHD[2],parseData.Frequency,parseData.Temperature.Oil_Temp.reduce(function(a,b){return (a+ b)},0),parseData.Temperature.Ambient_Temp,parseData.Temperature.OnChip]);
        } catch (err) {
            console.error(err.message);
        } 
        socket.emit('data', {
            message: JSON.parse(message),
            topic: topic
        })
    });

    client.on("connect", () => {
        client.subscribe(topic);
    });
        
})  */