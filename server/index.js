const express = require("express");
const app = express();
const cors = require("cors");
const pool = require("./db");
const fetch = require('node-fetch');
const http = require('http');
const socketIo = require('socket.io');
const bodyParser = require('body-parser');
const { parse } = require("path");
const fs = require('fs');
const schedule = require('node-schedule');
var caFile = fs.readFileSync('my_root_ca.pem');
var certFile = fs.readFileSync('client.pem');
var keyFile = fs.readFileSync('client.key'); 
const Sqreen = require('sqreen');
const authorization = require("./middleware/authorization");
//middleware

app.use(cors());
app.use(express.json());
app.use( bodyParser.json() );  // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({ // to support URL-encoded bodies
  extended: true
}));
app.use(Sqreen.middleware)

//ROUTES
app.use("/auth", require("./routes/jwtAuth"));
app.use("/dashboards", require("./routes/dashboards"));
app.use("/chart", require("./routes/chart"));
app.use("/weather", require("./routes/weather"));
app.use("/broker", require("./routes/broker"));
app.use("/alert", require("./routes/alert"));
app.use("/test", require("./routes/fameapi"));

//Create
app.post("/DTMS", async(req,rest)=> {
    try {
        
        console.log(req.body);
        rest.send('pogi mo duane');
    } catch (err) {
        //console.error(err.message);
    }
})

//Get

//Update

//Delete


/* app.listen(5000, () =>{
    console.log("Server Listening to Port 5000")
}); */

var MQTToptions={
    clientId:"Duane",
    port:8883,
    host:'103.219.69.55',
    protocol:'mqtts',
    rejectUnauthorized : false,
    ca:caFile,
    cert: certFile,
    key: keyFile
}
var Serveroptions = {
    cert: certFile,
    key: keyFile
}

const api_url =  "http://103.219.69.55:5000/broker/"; 
const server = http.Server(app);
 const io = socketIo(server,  {
    cors: {
        origin: "iot.ionics-ems.com:8080",
        methods: ["GET", "POST"]
      }
  });
 
/* const { connect } = require("mqtt");
var mqtt = require("mqtt");
var client = mqtt.connect(options);
var topic = "/meralco/update/+/status";

client.on("message", (topic,message) => {
    io.on('connection', (socket) => {
        console.log(message.toString)
        socket.emit('active',message.toString)
    })
});

client.on("connect", () => {
    client.subscribe(topic);
});  */

function unixTime(unixtime) {

    var u = new Date(unixtime*1000);

      return u.getUTCFullYear() +
        '-' + ('0' + u.getUTCMonth()).slice(-2) +
        '-' + ('0' + u.getUTCDate()).slice(-2) + 
        ' ' + ('0' + u.getUTCHours()).slice(-2) +
        ':' + ('0' + u.getUTCMinutes()).slice(-2) +
        ':' + ('0' + u.getUTCSeconds()).slice(-2) +
        '.' + (u.getUTCMilliseconds() / 1000).toFixed(3).slice(2, 5) 
};
const topic1 = "meralco/update/+/status"; // meralco/# wildcard  meralco/update/+/ like
const topic2 = "meralco/update/+/values";
const topic3 = "Meralco/update/+/values";
const topic4 = "Meralco/update/+/status";
const topictest = "meralco/test";

 
    io.on('connection', (socket) => {
    socket.on('disconnect', () => {});
    function emitstat(stat,tln){
        socket.broadcast.emit('active',{
            stat: stat,
            tln: tln
        })
    }

    function emitvalue(value,tln){
        socket.broadcast.emit('value',{
            value: value,
            tln: tln
        })
    }
    const offtln = require('./routes/schedule')
    // console.log(offtln.idletln)
    app.use("/sched", require("./routes/schedule"));
    const { connect } = require("mqtt");
    var mqtt = require("mqtt");
    var client = mqtt.connect(MQTToptions);
    const pool = require("./db");
    client.on("message", async (topic,message) => {
        console.log(JSON.parse(message))
        console.log(topic)
        
        if(topic.split("/")[3]==='values')
        {
            try {
                
            console.log('Message format \nUUID: ' + topic.split("/")[2])
            console.log(JSON.parse(message))
            const tln = await pool.query("SELECT a.tln,a.phase,b.kva,a.mqtt_interval,a.https_interval from bendpoint_dtms a inner join loc_dtms b on a.tln = b.tln where a.uuid =$1",[topic.split("/")[2]])
            
            const parseData = await JSON.parse(message)
            var A = parseData.Current.reduce(function(a,b){return (a+ b)},0)
            var V = parseData.Voltage.reduce(function(a,b){return (a+ b)},0)
            var d = new Date(parseData.TS*1000)
            var fd = d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds() 
            var KVA = parseInt(tln.rows[0].phase) === 0 ? ((V*A)/(1000*1.732)) : ((V*A)/1000)
            var lvab = ((parseData.Voltage[0] + parseData.Voltage[1]) / 2) * 1.732 // line voltage ab
            var lvbc = ((parseData.Voltage[1] + parseData.Voltage[2]) / 2) * 1.732 // line voltage bc
            var lvca = ((parseData.Voltage[0] + parseData.Voltage[2]) / 2) * 1.732 // line voltage ca
            var lcab = ((parseData.Current[0] + parseData.Current[1]) / 2) // line current ab
            var lcbc = ((parseData.Current[1] + parseData.Current[2]) / 2) // line current bc
            var lcca = ((parseData.Current[0] + parseData.Current[2]) / 2) // line current ca
            var actotal = parseData.AcPower[0] + parseData.AcPower[1] + parseData.AcPower[2]
            var retotal = parseData.RePower[0] + parseData.RePower[1] + parseData.RePower[2]
            var aptotal = parseData.ApPower[0] + parseData.ApPower[1] + parseData.ApPower[2]
            console.log(fd)
            
            if(tln.rowCount > 0)
            {
                    try {
                        pool.query("INSERT INTO chart4_DTMS (tln, main_topic, uuid, voltagep1, voltagep2, voltagep3,voltageab ,voltagebc,voltageca, currentp1, currentp2, currentp3,currentab,currentbc,currentca , acpower1, acpower2, acpower3, acpowertotal, repower1, repower2, repower3, repowertotal, appower1, appower2,appower3, appowertotal,pfactorp1,pfactorp2,pfactorp3, vthdp1, vthdp2,vthdp3, cthdp1, cthdp2,cthdp3, frequency, body_temp,ambient_temp, onchip, ts) VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19,$20,$21,$22,$23,$24,$25,$26,$27,$28,$29,$30,$31,$32,$33,$34,$35,$36,$37,$38,$39,$40,$41) RETURNING *",[tln.rows[0].tln,topic.split("/")[0],parseData.UUID,parseData.Voltage[0],parseData.Voltage[1],parseData.Voltage[2],lvab,lvbc,lvca,parseData.Current[0],parseData.Current[1],parseData.Current[2],lcab,lcbc,lcca,parseData.AcPower[0],parseData.AcPower[1],parseData.AcPower[2],actotal,parseData.RePower[0],parseData.RePower[1],parseData.RePower[2],retotal,parseData.ApPower[0],parseData.ApPower[1],parseData.ApPower[2],aptotal,parseData.PFactor[0],parseData.PFactor[1],parseData.PFactor[2],parseData.VTHD[0],parseData.VTHD[1],parseData.VTHD[2],parseData.CTHD[0],parseData.CTHD[1],parseData.CTHD[2],parseData.Frequency,parseData.Temperature[0].Body_Temp,parseData.Temperature[0].Ambient_Temp,parseData.Temperature[0]['On-Chip'],fd]);
                        /*                      pool.query("INSERT INTO chart2_DTMS (tln, main_topic, uuid, voltagep1, voltagep2, voltagel, currentp1, currentp2, currentl, faenergyp1, faenergyp2, faenergytotal, frenergyp1, frenergyp2, frenergytotal, rrenergyp1, rrenergyp2, rrenergytotal, aenergyp1, aenergyp2, aenergyptotal, pfactorp1, pfactorp2, vthdp1, vthdp2, cthdp1, cthdp2, frequency, body_temp, ambient_temp, onchip, ts) VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19,$20,$21,$22,$23,$24,$25,$26,$27,$28,$29,$30,$31,$32) RETURNING *",[tln.rows[0].tln,topic.split("/")[0],parseData.UUID,parseData.Voltage[0],parseData.Voltage[1],parseData.Voltage[2],parseData.Current[0],parseData.Current[1],parseData.Current[2],parseData.FAEnergy[0],parseData.FAEnergy[1],parseData.FAEnergy[2],parseData.FREnergy[0],parseData.FREnergy[1],parseData.FREnergy[2],parseData.RREnergy[0],parseData.RREnergy[1],parseData.RREnergy[2],parseData.AEnergy[0],parseData.AEnergy[1],parseData.AEnergy[2],parseData.PFactor[0],parseData.PFactor[1],parseData.VTHD[0],parseData.VTHD[1],parseData.CTHD[0],parseData.CTHD[1],parseData.Frequency,parseData.Temperature[0].Body_Temp,parseData.Temperature[0].Ambient_Temp,parseData.Temperature[0]['On-Chip'],fd]); */                        
                        const pload = ((aptotal / tln.rows[0].kva) * 100).toFixed(2) // KVA Percentage
                        var stat
                        if(parseFloat(pload) < 31)
                        {
                            stat = 'Under Load'
                        }
                        else if(parseFloat(pload)>=31 && parseFloat(pload)<81)
                        {
                            stat = 'Normal Load'
                        }
                        else if (parseFloat(pload) >=81 && parseFloat(pload) <=131)
                        {
                            stat = 'Critical Load'
                        }
                        else if (parseFloat(pload) >131)
                        {
                            stat = 'Over Load'
                        }
                        /* console.log("TLN:" + tln.rows[0].tln + ' KVA: ' + tln.rows[0].kva)
                        console.log("Percent Load:" + pload + ' KVA: ' + tln.rows[0].kva)
                        console.log(stat) */
                        var vol = (lvab + lvbc + lvca) / 3 // line voltage average
                        pool.query("INSERT INTO logs_dtms (tln,uuid,kvarating,voltage,current,pload,duration,status,lastupdate) VALUES ($1,$2,$3,$4,$5,$6,$7,$8,now()) RETURNING *",[tln.rows[0].tln,topic.split("/")[2],tln.rows[0].kva,vol,parseData.Current[2],pload,tln.rows[0].mqtt_interval,stat]);
                        pool.query("UPDATE loc_dtms SET voltage = $1,percentload=$2,temp=$3,lastactive=now(),atemp=$5,ctemp=$6,status=true where tln = $4",[vol,pload,parseData.Temperature[0].Body_Temp,tln.rows[0].tln,parseData.Temperature[0].Ambient_Temp,parseData.Temperature[0]['On-Chip']])
                    } catch (err) {
                    //console.error('Format Error!');
                }
            }
            else
            {
                console.log('UUID does not exist!')
            }
            
            var PL = (aptotal/ parseData.Voltage[2]) * 100
            var value = {
                KVA:aptotal,
                BodyTemp: parseData.Temperature[0].Body_Temp,
                AmbientTemp: parseData.Temperature[0].Ambient_Temp,
                OnChip: parseData.Temperature[0]['On-Chip'],
                PL:PL,
            }
            emitvalue(value,tln.rows[0].tln)
            emitstat(true,tln.rows[0].tln)
            //console.log(KVA)
            //console.log(JSON.parse(message))
            
            //console.log(parseData)
            } catch (error) {
                console.log('Message format not accepted!\nUUID: ' + topic.split("/")[2])
            }
        }
        else if(topic.split("/")[3]==='status')
        {
            const status = JSON.parse(message)
            const tln = await pool.query("SELECT tln from bendpoint_dtms where uuid =$1",[topic.split("/")[2]])
            //const stat = status.stat === 'online' ? true : false
            try {
                pool.query("UPDATE loc_dtms set status = $1 where tln = $2",[status.stat,tln.rows[0].tln])
            } catch (err) {
                //console.error(err.message)
            }
            /* console.log(tln.rows[0].tln)
            console.log(JSON.parse(message),tln.rows[0].tln)
            console.log(stat) */
            emitstat(status.stat,tln.rows[0].tln)
        }
         
    });
    client.on("connect", () => {
        client.subscribe(topic1);
    });

    client.on("connect", () => {
        client.subscribe(topic2);
    });
    client.on("connect", () => {
        client.subscribe(topictest);
    });
    client.on("connect", () => {
        client.subscribe(topic3);
    });
    client.on("connect", () => {
        client.subscribe(topic4);
    });
})


server.listen(5000, () =>{
    console.log("Server Listening to Port 5000")
});



        
 