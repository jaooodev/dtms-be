module.exports = (req,res,next) => {
    const {user_name, user_fname, user_lname, user_email, user_password } = req.body;
    function validEmail(user_email)
    {
        return /^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(user_email);
    }
    if(req.path ==="/register")
    {
        //console.log(!user_email.length);
        if(![user_name, user_fname, user_lname, user_email, user_password].every(Boolean))
        {
            return res.status(401).json("Please complete details!");
        }
        else if(!validEmail(user_email))
        {
            return res.status(401).json("Invalid Email!");
        }
    }
    else if(req.path==="/login")
    {
        if(![user_name, user_password].every(Boolean))
        {
            return res.status(401).json("Missing Credentials!");
        }
    }
  

    next();
};