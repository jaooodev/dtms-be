module.exports = (req,res,next) => {
    const {tln, phase, main_topic, endpoint, port,ca,client_cert,client_key,con_sec,mqtt_interval,https_interval,uuid } = req.body;
    if(req.path ==="/register")
    {
        if(![tln, phase, main_topic, endpoint, port,ca,client_cert,client_key,con_sec,mqtt_interval,https_interval,uuid].every(Boolean))
        {
            return res.status(401).send({
                error: "Please Input all fields"
            })
        }
    }

    next();
};