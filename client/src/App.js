import React,{ Fragment , useState,useEffect } from 'react';
import './App.css';
import {BrowserRouter as Router, Switch,Route,Redirect} from "react-router-dom";
import {toast} from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
//components 

import Add from "./components/add";
import Dashboard from "./components/dashboard";
import Login from "./components/login";
import Register from "./components/register";
import Navbar from "./components/Navbar";



toast.configure();

function App() {

  const [isAuth, setisAuth] = useState(false);
  const setAuth = (boolean) => {
      setisAuth(boolean);
  };

  async function isAuthenticated() {

    try {
      const response = await fetch("http://localhost:5000/auth/isverify", {
        method:"GET",
        headers: {token : localStorage.token}
      });

      const parseRes = await response.json();
      parseRes === true ? setisAuth(true) : setisAuth(false);
    } catch (err) {
      console.error(err.message);
    }

  };

  useEffect(() => {
    isAuthenticated();
  });

  return (
    <Fragment> 
        <Router>
          
          <Navbar />
          <div className="container">
            <Switch>
              
              <Route exact path ="/" render={props => !isAuth ? <Login {...props} setAuth={setAuth} /> : <Redirect to="/dashboard"/>}/>
              <Route exact path ="/login" render={props => !isAuth ? <Login {...props} setAuth={setAuth} /> : <Redirect to="/dashboard"/>}/>
              <Route exact path ="/dashboard" render={props => isAuth ?  <Dashboard {...props} setAuth={setAuth} /> : <Redirect to="/login"/>}/>
              <Route exact path ="/register" render={props => !isAuth ? <Register {...props} setAuth={setAuth} /> : <Redirect to="/dashboard"/>}/>
              <Route exact path ="/add" render={props => isAuth ?  <Add {...props} setAuth={setAuth} /> : <Redirect to="/login"/>}/>
              
            </Switch>
          </div>
        </Router>
    </Fragment>
  );
}

export default App;
