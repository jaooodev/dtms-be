import React, { Fragment, useState } from "react";
import {Link} from "react-router-dom";
import {toast} from "react-toastify";


const Login = ({setAuth}) => {
    const [inputs, setInputs] = useState({
        user_name:"",
        user_password:""
    });
    const {user_name,user_password} = inputs;

    const onChange  = e =>{
        setInputs({...inputs, [e.target.name]: e.target.value});
    }
    const onSubmitForm = async (e) => {
        e.preventDefault();
        try {

                
                const body = { user_name,user_password };
                const response = await fetch("http://localhost:5000/auth/login", {
                method: "POST",
                headers: {"Content-Type" : "application/json"},
                body: JSON.stringify(body)
                });

                const parseRes = await response.json();
                if(parseRes.token)
                {
                    localStorage.setItem("token", parseRes.token);
                    localStorage.setItem("name", user_name);
                    setAuth(true);
                    toast.success("Login Successfully!") 
                }
                else{
                    setAuth(false);
                    toast.error(parseRes);
                }
                 
                
            
            
        } catch (err) {
            console.error(err.message);
        }
    };

    return (
        <Fragment>
            <br/>
            <main class="mdl-layout__content mdl-color--gray-100 text-dark ">
                    <div class="mdl-grid demo-content ">
                        <div class="demo-charts mdl-color--gray mdl-shadow--2dp mdl-cell mdl-cell--12-col mdl-grid" >
                            <h1>Login</h1>
                            <form class="col-md-6" onSubmit={onSubmitForm}>
                                <div class="form-group" >
                                    <label for="pwd">Username:</label>
                                    <input type="text" class="form-control" id="user_name" name="user_name" placeholder="Enter Username" value={user_name} onChange={e => onChange(e)}/>
                                </div>

                                <div class="form-group" >
                                    <label for="pwd">Password:</label>
                                    <input type="password" class="form-control" id="user_password" name="user_password" placeholder="Enter Password" value={user_password} onChange={e => onChange(e)}/>
                                </div>

                               
                                <div class="panel-footer text-right">
                                    <button type ="submit" name="btnsubmit" id="btnsubmit" class="btn btn-success btn-lg">LOGIN</button>
                                </div><br/>
                            </form>
                            <Link to="/register">Register</Link>
                        </div>
                    </div>
                </main>

        </Fragment>
    )
}

export default Login;