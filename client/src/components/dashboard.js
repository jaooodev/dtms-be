import React, { Fragment, useState, useEffect, Component } from "react";
import {Link} from "react-router-dom";
import {toast} from "react-toastify";
import L from 'leaflet';
import {Map, TileLayer, Marker, Popup, useLeaflet} from 'react-leaflet';
import { render } from "react-dom";
import "leaflet/dist/leaflet.css";



var url = "http://localhost:5000/";
const Dashboard = ({setAuth}) => {

    const [Details, SetUserDetails] = useState("");
    const [PostDetails, SetPostDetails] = useState([]);
    const [Coords, SetCoords] = useState([]);
    

    async function getname(){
        try {
            const response = await fetch(url +"dashboards/", {
                method:"GET",
                headers:{token:localStorage.token}
            });

          

            const parseRes = await response.json();
         
            SetUserDetails({ 
                uname: parseRes.user_name, 
                fname: parseRes.user_fname
            });

          
            
            //console.log(parseRes);
        } catch (err) {
            console.error(err.message);
        }
    };

   const getpost = async () => {
        try {
            const response = await fetch(url +"dashboards/post");
            const parseRes = await response.json();
            SetPostDetails(parseRes);
            
            
            console.log(parseRes);
        } catch (err) {
            console.error(err.message);
        }
    };

    useEffect(() => {
        getname();
        getpost();
    },[]);


    const logout = (e) => {
       e.preventDefault();
       localStorage.removeItem("token");
       localStorage.removeItem("name");
       setAuth(false);
       toast.success("Logged out Successfully!");
    };

    const position = [14.1407,121.7740];
        

    L.Icon.Default.mergeOptions({
        iconRetinaUrl: require('leaflet/dist/images/marker-icon-2x.png'),
        iconUrl: require('leaflet/dist/images/marker-icon.png'),
        shadowUrl: require('leaflet/dist/images/marker-shadow.png'),
      });

    function GetIcon(_iconSize){
        return L.icon({
            iconUrl: require('leaflet/dist/images/marker-icon.png'),
            iconSize: _iconSize
        })
    };

   
    
    return (
        <Fragment>
            <h1>Welcome {Details.uname} {Details.fname}{Coords.tln}{PostDetails.length} {position}</h1>
            {PostDetails.map((PDetails) => (
                <p>{PDetails.tln} {PDetails.lat}{PDetails.lng}{PDetails.loc}</p>
                
                
            ))}
            <div id ="mymap">
                
            </div>
            
            <div class="panel-footer text-right">
                    <button type ="submit" name="btnsubmit" id="btnsubmit" class="btn btn-primary btn-lg" onClick={e => logout(e)}>Logout</button>
            </div><br/>
            <Link to="/add"><button type ="button" name="btnadd" id="btnadd" class="btn btn-primary btn-lg">Add Post</button></Link>
            <br/>
        </Fragment>
    )
    
    
    
}

export default Dashboard;