import React, { Fragment, useState } from "react";
import {Link} from "react-router-dom";
import { toast } from "react-toastify";
const Register = ({setAuth}) => {

    const [inputs, setInputs] = useState({
        user_name:"",
        user_fname:"",
        user_lname:"",
        user_email:"",
        user_password:"",
        user_password2:""
    });

    const {user_name,user_fname,user_lname,user_email,user_password,user_password2} = inputs;

    const onChange  = e =>{
        setInputs({...inputs, [e.target.name]: e.target.value});
    }

    const onSubmitForm = async (e) => {
        e.preventDefault();
        try {

                console.log(user_password,user_password2)
                const body = { user_name,user_fname,user_lname,user_email,user_password };
                const response = await fetch("http://localhost:5000/auth/register", {
                method: "POST",
                headers: {"Content-Type" : "application/json"},
                body: JSON.stringify(body)
                });

                const parseRes = await response.json();
                if(parseRes.token)
                {
                    localStorage.setItem("token", parseRes.token);
                    setAuth(true);
                    toast.success("Registered Successfully");
                }
                else
                {
                    toast.error(parseRes);
                    setAuth(false);
                }
                
                
            
            
        } catch (err) {
            console.error(err.message);
        }
    };

    return (
        <Fragment>
            
                <main class="mdl-layout__content mdl-color--gray-100 text-dark">
                    <div class="mdl-grid demo-content">
                        <div class="demo-charts mdl-color--gray mdl-shadow--2dp mdl-cell mdl-cell--12-col mdl-grid"  >
                            <h1>Register</h1>
                            <form class="col-md-6" onSubmit={onSubmitForm}>
                                <div class="form-group" >
                                    <label for="pwd">Username:</label>
                                    <input type="text" class="form-control" id="user_name" name="user_name" placeholder="Enter Username" value={user_name} onChange={e => onChange(e)}/>
                                </div>

                                <div class="form-group" >
                                    <label for="pwd">Firstname:</label>
                                    <input type="text" class="form-control" id="user_fname" name="user_fname" placeholder="Enter First name" value={user_fname} onChange={e => onChange(e)}/>
                                </div>

                                <div class="form-group" >
                                    <label for="pwd">Lastname:</label>
                                    <input type="text" class="form-control" id="user_lname" name="user_lname" placeholder="Enter Last name" value={user_lname} onChange={e => onChange(e)}/>
                                </div>

                                <div class="form-group" >
                                    <label for="pwd">Email:</label>
                                    <input type="email" class="form-control" id="user_email" name="user_email" placeholder="Enter Email Address" value={user_email} onChange={e => onChange(e)}/>
                                </div>

                                <div class="form-group" >
                                    <label for="pwd">Password:</label>
                                    <input type="password" class="form-control" id="user_password" name="user_password" placeholder="Enter Password" value={user_password} onChange={e => onChange(e)}/>
                                </div>

                                <div class="form-group" >
                                    <label for="pwd">Confirm Password:</label>
                                    <input type="password" class="form-control" id="user_password2" name="user_password2" placeholder="Confirm Password" value={user_password2} onChange={e => onChange(e)}/>
                                </div>

                                <div class="panel-footer text-right">
                                    <button type ="submit" name="btnsubmit" id="btnsubmit" class="btn btn-success btn-lg">REGISTER</button>
                                </div><br/>
                            </form>
                            <Link to="/login">Login</Link>
                        </div>
                    </div>
                </main>
            
        </Fragment>
    )
}

export default Register;