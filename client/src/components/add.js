import React, { Fragment, useState } from "react";
import {Link} from "react-router-dom";
import { toast } from "react-toastify";


const Add = ({setAuth}) => {

    const [inputs, setInputs] = useState({
        serialno:"",
        tln:"",
        tcn:"",
        circuitno:"",
        area:"",
        sector:"",
        lat:"",
        lng:"",
        loc:"",
        installed:"",
        energized:"",
        installedby:"",
        energizedby:"",
        lastactiveby:localStorage.getItem("name")
    });

    const {serialno, tln, tcn, circuitno, area,sector,lat,lng,loc,installed,energized,installedby,energizedby,lastactiveby} = inputs;
    
    const onChange  = e =>{
        setInputs({...inputs, [e.target.name]: e.target.value});
    }

    const onSubmitForm = async (e) => {
        e.preventDefault();
        try {

                console.log(localStorage.getItem("name"));
                const body = { serialno, tln, tcn, circuitno, area,sector,lat,lng,loc,installed,energized,installedby,energizedby,lastactiveby };
                const response = await fetch("http://localhost:5000/dashboards/add", {
                method: "POST",
                headers: {"Content-Type" : "application/json"},
                body: JSON.stringify(body)
                });
                
                const parseRes = await response.json();
                if(parseRes.status)
                {
                    toast.error(parseRes.error);
                }
                else
                {
                    toast.error("Successfully Added!");
                }
                
                
              
                
            
            
        } catch (err) {
            console.error(err.message);
        }
    };

    return (
        <Fragment>
            
                <main class="mdl-layout__content mdl-color--gray-100 text-dark">
                    <div class="mdl-grid demo-content">
                        <div class="demo-charts mdl-color--gray mdl-shadow--2dp mdl-cell mdl-cell--12-col mdl-grid"  >
                            <h1>Register</h1>
                            <form class="col-md-6" onSubmit={onSubmitForm}>
                                <div class="form-group" >
                                    <label for="pwd">Serial Number:</label>
                                    <input type="text" class="form-control" id="serialno" name="serialno" placeholder="Enter Username" value={serialno} onChange={e => onChange(e)}/>
                                </div>

                                <div class="form-group" >
                                    <label for="pwd">TLN:</label>
                                    <input type="text" class="form-control" id="tln" name="tln" placeholder="Enter First name" value={tln} onChange={e => onChange(e)}/>
                                </div>

                                <div class="form-group" >
                                    <label for="pwd">TCN:</label>
                                    <input type="text" class="form-control" id="tcn" name="tcn" placeholder="Enter Last name" value={tcn} onChange={e => onChange(e)}/>
                                </div>

                                <div class="form-group" >
                                    <label for="pwd">Circuit No:</label>
                                    <input type="text" class="form-control" id="circuitno" name="circuitno" placeholder="Enter Last name" value={circuitno} onChange={e => onChange(e)}/>
                                </div>

                                <div class="form-group" >
                                    <label for="pwd">Area:</label>
                                    <input type="text" class="form-control" id="area" name="area" placeholder="Enter Last name" value={area} onChange={e => onChange(e)}/>
                                </div>

                                <div class="form-group" >
                                    <label for="pwd">Sector:</label>
                                    <input type="text" class="form-control" id="sector" name="sector" placeholder="Enter Last name" value={sector} onChange={e => onChange(e)}/>
                                </div>

                                <div class="form-group" >
                                    <label for="pwd">Latitude:</label>
                                    <input type="text" class="form-control" id="lat" name="lat" placeholder="Enter Last name" value={lat} onChange={e => onChange(e)}/>
                                </div>

                                <div class="form-group" >
                                    <label for="pwd">Longitude:</label>
                                    <input type="text" class="form-control" id="lng" name="lng" placeholder="Enter Last name" value={lng} onChange={e => onChange(e)}/>
                                </div>

                                <div class="form-group" >
                                    <label for="pwd">Location:</label>
                                    <input type="text" class="form-control" id="loc" name="loc" placeholder="Enter Last name" value={loc} onChange={e => onChange(e)}/>
                                </div>

                                <div class="form-group" >
                                    <label for="pwd">Installed By:</label>
                                    <input type="text" class="form-control" id="installedby" name="installedby" placeholder="Enter Last name" value={installedby} onChange={e => onChange(e)}/>
                                </div>

                                <div class="form-group" >
                                    <label for="pwd">Installed Date:</label>
                                    <input type="date" class="form-control" id="installed" name="installed" placeholder="Enter Last name" value={installed} onChange={e => onChange(e)}/>
                                </div>

                                <div class="form-group" >
                                    <label for="pwd">Energized By:</label>
                                    <input type="text" class="form-control" id="energizedby" name="energizedby" placeholder="Enter Last name" value={energizedby} onChange={e => onChange(e)}/>
                                </div>

                                <div class="form-group" >
                                    <label for="pwd">Energized Date:</label>
                                    <input type="date" class="form-control" id="energized" name="energized" placeholder="Enter Last name" value={energized} onChange={e => onChange(e)}/>
                                </div>

                                

                                <div class="panel-footer text-right">
                                    <button type ="submit" name="btnsubmit" id="btnsubmit" class="btn btn-success btn-lg">Submit</button>
                                </div><br/>
                            </form>
                            <Link to="/dashboard">Home Page</Link>
                        </div>
                    </div>
                </main>
            
        </Fragment>
    )
}

export default Add;