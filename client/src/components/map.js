import React, { Fragment, useState, useEffect, Component } from "react";
import {Link} from "react-router-dom";
import {toast} from "react-toastify";
import L from 'leaflet';
import { render } from "react-dom";
import {Map, TileLayer, Marker, Popup} from 'react-leaflet';
import "leaflet/dist/leaflet.css";



const map = L.map('mymap').setView([14.1407,121.7740], 6);
const attribution='&copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors';
const tileUrl="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png";
const tiles = L.tileLayer(tileUrl,{ attribution });
tiles.addTo(map); 


getData();

async function getData(){
    const response = await fetch("http://103.219.69.55:5000/dashboards/post");
    const parseRes = await response.json();

    for(const item of parseRes)
    {
        console.log(item.lat);
    }
}



