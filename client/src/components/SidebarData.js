import React from 'react'
import * as FaIcons from 'react-icons/fa'
import * as AiIcons from 'react-icons/ai'
import * as IoIcons from 'react-icons/io'

export const SidebarData = 
[
    {
        title:"Home",
        path: '/',
        icon: <AiIcons.AiFillHome />,
        cName: "nav-text"
    },
    {
        title:"Post",
        path: '/post',
        icon: <AiIcons.AiFillThunderbolt />,
        cName: "nav-text"
    },
    {
        title:"Reports",
        path: '/reports',
        icon: <AiIcons.AiOutlineAreaChart />,
        cName: "nav-text"
    },
    {
        title:"Map",
        path: '/map',
        icon: <AiIcons.AiOutlineAreaChart />,
        cName: "nav-text"
    },
]